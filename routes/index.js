var express = require('express');
var router = express.Router();
const Student = require('../models/Student')
const scoreboard = require('../appp/scoreboard')



router.get('/students', async (request, response) => {
  try {
    const students = await Student.find().exec()
    response.send(students)
  } catch (error) {
    response.status(500).send(error);
  }
})

router.post('/students', async (request, reponse) => {
  try {
    const student = new Student(request.body)
    console.log(request.body)
    const result = await student.save()
    reponse.send(result)
  }
  catch (error) {
    response.status(500).send(error);
  }
})

router.get('/', async (request, response) => {
  try {
    const todayGames = await scoreboard.getTodayGames();
    response.send(todayGames);
  } catch (error) {
    response.send("small API error");
  }
})

module.exports = router;
